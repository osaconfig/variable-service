package main

import (
	"context"
	"log"

	varPb "gitlab.com/osaconfig/protobufs/variables"
	mgo "gopkg.in/mgo.v2"
)

// Our gRPC service handler
type service struct {
	session *mgo.Session
}

func (s *service) GetRepo() Repository {
	return &VariableRepository{s.session.Clone()}
}

// GetGroupVars - handler for GetGroupVars repo function
func (s *service) GetGroupVars(ctx context.Context, req *varPb.GetRequest, res *varPb.Response) error {
	log.Println("handler:GetGroupVars:", req)
	defer s.GetRepo().Close()
	// Find the environment
	groupVars, err := s.GetRepo().GetGroupVars(req)
	if err != nil {
		return err
	}
	res.GroupVars = groupVars
	return nil
}

// CreateGroupVars - handler for CreateGroupVars repo function
func (s *service) CreateGroupVars(ctx context.Context, req *varPb.GroupVars, res *varPb.Response) error {
	log.Println("handler:CreateGroupVars:", req)
	defer s.GetRepo().Close()
	if err := s.GetRepo().CreateGroupVars(req); err != nil {
		return err
	}
	res.GroupVars = req
	res.Created = true
	return nil
}

// RemoveGroupVars - handler for RemoveGroupVars repo function
func (s *service) RemoveGroupVars(ctx context.Context, req *varPb.GroupVars, res *varPb.Response) error {
	log.Println("handler:RemoveGroupVars:", req)
	defer s.GetRepo().Close()
	if err := s.GetRepo().RemoveGroupVars(req); err != nil {
		return err
	}
	res.GroupVars = req
	res.Removed = true
	return nil
}

package main

import (
	"fmt"
	"log"
	"os"

	"github.com/micro/go-micro"
	k8s "github.com/micro/kubernetes/go/micro"
	varPb "gitlab.com/osaconfig/protobufs/variables"
)

const (
	defaultHost = "localhost:27017"
	varsPath    = "./osaDefaultVariables/steinVars/"
)

func main() {
	// Database host from environment variables set in Dockerfile or spawining process
	host := os.Getenv("DB_HOST")
	// Fallback on defaultHost
	if host == "" {
		host = defaultHost
	}

	// Setup the datastore session
	session, err := CreateSession(host)
	log.Println("main:session init:", session)
	if err != nil {
		log.Panicf("Could not connect to datastore with host %s - %v", host, err)
	}
	defer session.Close()

	osaVariables := OSAVars{}

	srv := k8s.NewService(
		micro.Name("osaconfig.variableservice"),
		micro.Version("latest"),
	)

	repo := &VariableRepository{session.Copy()}
	log.Println("main:call:loadVariables:repo:", repo)
	osaVariables.loadVariables(repo)

	srv.Init()
	varPb.RegisterVariableServiceHandler(srv.Server(), &service{session})
	log.Println("main:service init:", srv)

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}

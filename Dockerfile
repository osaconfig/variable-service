FROM golang:1.12.0 as builder

WORKDIR /go/src/gitlab.com/osaconfig/variable-service

COPY . .

RUN curl -Ss https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
RUN dep ensure -v
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o variable-service .

FROM alpine:latest

RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/osaconfig/variable-service .

CMD ["./variable-service"]

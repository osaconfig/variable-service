UNAME_S := $(shell uname -s)
SEDCMD := sed -i ''      # MacOS sed need a place holeder for the inplace edit backup file
ifeq ($(UNAME_S),Linux)  # Linux does not need the placeholder and errors out if it's there
	SEDCMD = sed -i
endif
POD=$(shell kubectl -n osaconfig get pods -l app=variable-service -o jsonpath="{.items[0].metadata.name}")
INVENTORY_SERVICE_IP=$(shell kubectl -n osaconfig get services -l app=variable-service -o jsonpath="{.items[0].spec.clusterIP}")

build:
	docker build -t registry.gitlab.com/osaconfig/variable-service:latest .
	docker push registry.gitlab.com/osaconfig/variable-service:latest

run:
	docker run -d \
		-p 8080 \
		-e MICRO_SERVER_ADDRESS=:8080 \
		-e MICRO_REGISTRY=mdns \
		registry.gitlab.com/osaconfig/variable-service:latest

deploy-minikube:
	sed "s/{{ DB_PASSWORD }}/$(shell kubectl get secret --namespace osaconfig datastore-mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)/g" ./deployments/deployment.tmpl > ./deployments/deployment.yml
	$(SEDCMD) "s/{{ UPDATED_AT }}/$(shell date)/g" ./deployments/deployment.yml
	kubectl replace -f ./deployments/deployment.yml

setup:
	sudo mkdir -p /var/run/secrets/kubernetes.io/serviceaccount
	sudo chgrp wheel /var/run/secrets/kubernetes.io/serviceaccount
	sudo chmod g+w /var/run/secrets/kubernetes.io/serviceaccount
	kubectl -n osaconfig -it exec $(POD) cat /var/run/secrets/kubernetes.io/serviceaccount/ca.crt > /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
	kubectl -n osaconfig -it exec $(POD) cat /var/run/secrets/kubernetes.io/serviceaccount/token > /var/run/secrets/kubernetes.io/serviceaccount/token
	kubectl -n osaconfig -it exec $(POD) cat /var/run/secrets/kubernetes.io/serviceaccount/namespace > /var/run/secrets/kubernetes.io/serviceaccount/namespace
	@echo "To set the environment variable for the variable service endpoint you can run:"
	@echo "export INVENTORY_SERVICE_IP=$(INVENTORY_SERVICE_IP)"

package main

import (
	"io/ioutil"
	"log"
	"os"

	"github.com/davecgh/go-spew/spew"
	varPb "gitlab.com/osaconfig/protobufs/variables"
	yaml "gopkg.in/yaml.v2"
)

// OSAVars - struct to map existing YAML to protobuf
type OSAVars struct {
	groupVars varPb.GroupVars
}

// IOReadDir reads the directory and returns a slice of yaml files
func IOReadDir(root string) ([]string, error) {
	log.Println("utility:IOReadDir:", root)
	var files []string
	fileInfo, err := ioutil.ReadDir(root)
	if err != nil {
		return files, err
	}

	for _, file := range fileInfo {
		if file.IsDir() {
			subFiles, err := ioutil.ReadDir(root + "/" + file.Name())
			if err != nil {
				return files, err
			}
			for _, subFile := range subFiles {
				files = append(files, file.Name()+"/"+subFile.Name())
			}
		} else {
			files = append(files, file.Name())
		}
	}
	return files, nil
}

func (varsStruct *OSAVars) populateVariables() {
	log.Println("utility:populateVariables")
	filesList, err := IOReadDir(varsPath)
	if err != nil {
		log.Panicf("Could not retreive list of vars files: %v", err)
	}
	for _, file := range filesList {
		log.Println("utility:populateVariables:open", spew.Sdump(varsPath+file))
		f, err := os.Open(varsPath + file)
		if err != nil {
			log.Fatalf("Can't open: %v", err)
		}
		defer f.Close()

		varYAML, err := ioutil.ReadAll(f)
		if err != nil {
			log.Fatalf("Can't read: %v", err)
		}
		var varFile interface{}
		err = yaml.Unmarshal(varYAML, &varFile)
		log.Println("utility:populateVariables:unmarshal", spew.Sdump(varFile))

	}
}

func (varsStruct *OSAVars) loadVariables(repo Repository) {
	log.Println("utility:loadVariables:", repo)
	varsStruct.populateVariables()
	defer repo.Close()
	variables := varsStruct.groupVars
	repo.RemoveGroupVars(&variables)
	repo.CreateGroupVars(&variables)
}

package main

import (
	"log"

	varPb "gitlab.com/osaconfig/protobufs/variables"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dbName             = "osaconfig"
	variableCollection = "variables"
)

// Repository -
type Repository interface {
	GetGroupVars(*varPb.GetRequest) (*varPb.GroupVars, error)
	CreateGroupVars(*varPb.GroupVars) error
	RemoveGroupVars(*varPb.GroupVars) error
	Close()
}

// VariableRepository -
type VariableRepository struct {
	session *mgo.Session
}

// GetGroupVars - returns the set of group variables for a given environment
func (repo *VariableRepository) GetGroupVars(environ *varPb.GetRequest) (*varPb.GroupVars, error) {
	log.Println("repository:GetGroupVars:", environ)
	var groupVars *varPb.GroupVars

	err := repo.collection().Find(bson.M{
		"environment": bson.M{"$eq": environ.Environment},
	}).One(&groupVars)

	if err != nil {
		return nil, err
	}

	return groupVars, nil
}

// CreateGroupVars - creates group variables for an environment
func (repo *VariableRepository) CreateGroupVars(groupVars *varPb.GroupVars) error {
	log.Println("repository:CreateGroupVars:", groupVars)
	return repo.collection().Insert(groupVars)
}

// RemoveGroupVars - removes group variables for an environment
func (repo *VariableRepository) RemoveGroupVars(groupVars *varPb.GroupVars) error {
	log.Println("repository:RemoveGroupVars:", groupVars)
	return repo.collection().Remove(bson.M{
		"environment": bson.M{"$eq": groupVars.Environment},
	})
}

// Close - close the session
func (repo *VariableRepository) Close() {
	log.Println("repository:Close")
	repo.session.Close()
}

func (repo *VariableRepository) collection() *mgo.Collection {
	log.Println("repository:collection")
	return repo.session.DB(dbName).C(variableCollection)
}
